import Classes.*;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Weapon;

import java.util.Scanner;

public class Character {


    public static void main(String[] args) {


        Player activePlayer = null;
        Weapon wepList[] = new Weapon[3];


        Scanner myChar = new Scanner(System.in);
        System.out.println("What class would you like? (Warrior, Mage, Rogue or Ranger)");
        String selected = myChar.nextLine();

        Scanner myName = new Scanner(System.in);
        System.out.println("Type hero name: ");

        String activeName = myName.nextLine();


        switch (selected) { //Checks what class is selected
            case "Ranger": //Every case does the same, just different class type
                activePlayer = new Ranger(activeName, 1, 1, 7, 1); //Creates a Ranger if that is what the user picked (Name, level, str, dex, intel)

                System.out.println("You now have a lvl 1 Ranger, Welcome " + activeName); //Confirms that the Ranger was made
                System.out.println("Attributes: " + "Str: " + activePlayer.getStr() + " Dex: " + activePlayer.getDex() + " Int: " + activePlayer.getIntel()); //Displays attributes

                break;
            case "Warrior":
                activePlayer = new Warrior(activeName, 1, 5, 2, 1);
                System.out.println("You now have a lvl 1 Warrior, Welcome " + activeName);
                System.out.println("Attributes: " + "Str: " + activePlayer.getStr() + " Dex: " + activePlayer.getDex() + " Int: " + activePlayer.getIntel());

                break;

            case "Rogue":
                activePlayer = new Rogue(activeName, 1, 2, 6, 1);
                System.out.println("You now have a lvl 1 Rogue, Welcome " + activeName);
                System.out.println("Attributes: " + "Str: " + activePlayer.getStr() + " Dex: " + activePlayer.getDex() + " Int: " + activePlayer.getIntel());
                break;

            case "Mage":
                activePlayer = new Mage(activeName, 1, 1, 1, 8);
                System.out.println("You now have a lvl 1 Mage, Welcome " + activeName);
                System.out.println("Attributes: " + "Str: " + activePlayer.getStr() + " Dex: " + activePlayer.getDex() + " Int: " + activePlayer.getIntel());

                break;


        }

        boolean active = true;
        while (active) {//Infinate loop untill false, sets to false if the user writes Quit
            Scanner command = new Scanner(System.in);//Action input
            System.out.println("What is you next action? (Write the option you want) \n Train \n Equip Wep \n Equip Armor \n Stats \n Quit");//All the actions available
            String com = command.nextLine();

            switch (com) {//Checks what action to execute
                case "Train":
                    activePlayer.levelUp(activePlayer); //Levels up the player
                    break;
                case "Equip Wep":


                        activePlayer.equipWep(activePlayer); //equips a weapon of the players choice

                    break;

                case "Equip Armor":
                        activePlayer.equipArmor(activePlayer); //Equips armor of the players choice


                    break;
                case "Stats": //Shows name, level, and the attributes for the player, aswell as the dps
                    System.out.println(activePlayer.getName() + " Level " + activePlayer.getLevel());

                    System.out.println("Str: " + activePlayer.getStr());
                    System.out.println("Dex: " + activePlayer.getDex());
                    System.out.println("Int: " + activePlayer.getIntel());
                    System.out.println("Dps :" + activePlayer.getDps());
                    break;

                case "Quit": //Ends the program
                    active = false;
                    break;
                default:
                    System.out.println("Please write the commands as they are listed");//If unknown command is entered, lets the user know to write them correctly

            }

        }
    }
}