package Classes;

import Items.Armor;
import Items.Weapon;


public class Mage extends Player{

    //Variables for mage
    int level = 1;
    int dps;
    int intel = 8;
    Weapon wepList[] = new Weapon[3];
    Armor  armorList[] = new Armor[10];
    Armor  equippedArmor[] = new Armor[5];



    //Constructor that creates weapons and armor for mage
    public Mage(String name, int level, int str, int dex, int intel){
        super(name, level, str, dex, intel);
        this.dps = (this.level*(1+(this.intel/100)));


        wepList[0] = new Weapon("Basic Wand", 1, "One-Hand", 10, 1, 1, 1, 5);//All weapons for mages
        wepList[1] = new Weapon("Advanced Wand", 5, "One-Hand", 40, 2, 1, 1, 10);
        wepList[2] = new Weapon("Master Wand", 10, "One-Hand", 100,3, 1, 1, 15);

        armorList[0] = new Armor("Gloves", "Cloth", 1, 1,1,3); //All armor existing for Mages
        armorList[1] = new Armor("Boots", "Cloth", 1, 1,1,3);
        armorList[2] = new Armor("Chest", "Cloth", 1, 1,1,5);
        armorList[3] = new Armor("Legs", "Cloth", 1, 1,1,5);
        armorList[4] = new Armor("Head", "Cloth", 1, 2,1,4);
        armorList[5] = new Armor("Gloves2", "Cloth", 5, 2,2,6);
        armorList[6] = new Armor("Boots2", "Cloth", 5, 2,2,6);
        armorList[7] = new Armor("Chest2", "Cloth", 5, 2,2,10);
        armorList[8] = new Armor("Legs2", "Cloth", 5, 2,2,10);
        armorList[9] = new Armor("Head2", "Cloth", 5, 2,2,8);


    }
    public int getDps(){
        return this.dps;
    }
    public void setDps(int i){
        this.dps = i;
    }

    public Weapon[] getWeaponList(){
        return this.wepList;
    }
    public Armor[] getArmorList(){
        return this.armorList;
    }
    public Armor[] getEquippedArmor(){
        return this.equippedArmor;
    }
    @Override
    public void levelUp(Player player){ //Upgrades the level of the player asewl as the attributes.
        player.setLevel((player.getLevel()+1));
        player.setDex(player.getDex() + 1);
        player.setStr(player.getStr() + 1);
        player.setIntel(player.getIntel() + 5);

        System.out.println("You have leveled up! Your are now lvl " + player.getLevel());
    }
}
