package Classes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.*;

import java.util.Scanner;

public abstract class Player implements methods {

    //Variables for a player
    private String name;
    private int level;
    private int str;
    private int dex;
    private Weapon weapon;
    private boolean hasWeapon;
    private Armor armor;
    private int intel;
    private int dps;
    Weapon[] wepList;
    Armor[] armorList;
    Armor equippedArmor[];

    //Constructor
    public Player(String name, int level, int str, int dex, int intel) {
        this.name = name;
        this.level = level;
        this.str = str;
        this.dex = dex;
        this.intel = intel;
        this.dps = getDps();



    }
    @Override
    public void equipWep(Player player) { //Tries to equip a weapon, returns exception with a message if not possible.
        boolean act = true; //Variable for keeping loop active until wep selected
        Weapon selectedWeapon = null; //will be the selected weapon after input
        while (act) { //Loops until an allowed weapon is selected
            wepList = getWeaponList();

            //Prints out a list of the available weapons
            System.out.println("You have " + wepList.length + " weapons in your inventory");
            System.out.println(wepList[0].getName() + "   Requires lvl " + wepList[0].getReqLvl());
            System.out.println(wepList[1].getName() + "   Requires lvl " + wepList[1].getReqLvl());
            System.out.println(wepList[2].getName() + "   Requires lvl " + wepList[2].getReqLvl());

            Scanner myWep = new Scanner(System.in); //Input for selecting weapon
            System.out.println("Type the name of the weapon you want, be aware of the lvl requirements!");
            String wepSelect = myWep.nextLine();

            for (Weapon w : wepList) {//Checks if any weapon name equals the given input
                if (w.getName().contains(wepSelect)) {

                    selectedWeapon = w;
                    break;
                }


            }

            try {
                act = equipWep2(player, selectedWeapon);
            } catch (InvalidWeaponException e) {
                act = false;
                System.out.println(e.getMessage());
            }
        }

    }

    @Override
    public void equipArmor(Player player){//Tries to equip armor, exception if not.
        boolean act = true; //Variable for keeping loop active until armor selected
        Armor selectedArmor = null; //will be the selected armor after input
        while (act) { //Loops until an allowed armor is selected
            armorList = getArmorList();
            System.out.println("You have " + armorList.length + " armor pieces in your inventory");

            for(int i = 0; i < armorList.length; i++){//Loop for displaying all armor for this class
                System.out.println(armorList[i].getName() + "   Requires lvl " + armorList[i].getLevel());

            }


            Scanner myArmor = new Scanner(System.in); //Input for selecting armor
            System.out.println("Type the name of the armor you want, be aware of the lvl requirements!");
            String armorselect = myArmor.nextLine(); //name of the selected armor

            for (Armor a : armorList) {//Checks if any armor name equals the given input
                if (a.getName().contains(armorselect)) {

                    selectedArmor = a;
                    break;
                }
            }

            try {
                act = equipArmor2(player, selectedArmor);
            }catch(InvalidArmorException e){
                act = false; //Stops the select armor loop, so that the user can decide on a new action.
                System.out.println(e.getMessage());
            }
        }

    }

    @Override
    public Armor getPreviousArmor(Armor armor){ //Loops through the array of slots, and finds the given armor
        equippedArmor = getEquippedArmor();
        if(armor.getName().contains("Glove")){
            return equippedArmor[0];
        }else if(armor.getName().contains("Boot")){
            return equippedArmor[1];
        }else if(armor.getName().contains("Chest")){
            return equippedArmor[2];
        }else if(armor.getName().contains("Head")){
            return equippedArmor[3];
        }else if(armor.getName().contains("Leg")){
            return equippedArmor[4];
        }else return null; //returns null if no armor was found
    }

    @Override
    public boolean isArmorEquipped(Armor armor){ //Loops through to see if the given armor is equiped
        equippedArmor = getEquippedArmor();
        for(int i = 0; i < equippedArmor.length; i++){

            if(equippedArmor[i] == null){
            }else{
                if(armor.getName().contains(equippedArmor[i].getName())){

                    return true;
                }

            }


        }
        return false;
    }

    @Override
    public void addArmor(Armor armor){ //Adds armor to array of slots
        equippedArmor = getEquippedArmor();
        if(armor.getName().contains("Glove")){
            this.equippedArmor[0] = armor;
        }else if(armor.getName().contains("Boot")){
            this.equippedArmor[1] = armor;
        }else if(armor.getName().contains("Chest")){
            this.equippedArmor[2] = armor;
        }else if(armor.getName().contains("Head")){
            this.equippedArmor[3] = armor;
        }else if(armor.getName().contains("Leg")){
            this.equippedArmor[4] = armor;
        }
    }

    //Helper method for equip armor, makes it easier to run tests etc.
    public boolean equipArmor2(Player player, Armor selectedArmor) throws InvalidArmorException{

        if(selectedArmor == null){
            throw new InvalidArmorException("Armor does no exist");
        }else if(player.getLevel() < selectedArmor.getLevel()){
            throw new InvalidArmorException("You do not have the level requirement for this armor");
        }

        else{//Checks requirements

            if(!isArmorEquipped(selectedArmor)){ //If no armor is in the slot, equips the armor.
                addArmor(selectedArmor);
                player.setArmor(selectedArmor);
                player.setStr((player.getStr()) + selectedArmor.getStr());
                player.setDex((player.getDex()) + selectedArmor.getDex());
                player.setIntel((player.getIntel()) + selectedArmor.getIntel());

                if(player.getWeapon() != null) {
                    player.setDps((player.getWeapon().getDps() * (1 + (player.getStr() / 100))));
                }else{ player.setDps((1 * (1 + (player.getStr() / 100))));
                }



                System.out.println("You have now equipped the armor piece: " + selectedArmor.getName());
                return false;


            }else{ //If armor slot has an item, removes the previous attributes and adds the new armor with attributes.
                Armor prevArmor = getPreviousArmor(selectedArmor);
                addArmor(selectedArmor);
                player.setStr((player.getStr() - prevArmor.getStr()));
                player.setDex((player.getDex() - prevArmor.getDex()));
                player.setIntel((player.getIntel() - prevArmor.getIntel()));
                if(player.getWeapon() != null) {
                    player.setDps((player.getWeapon().getDps() * (1 + (player.getStr() / 100))));
                }else{ player.setDps((1 * (1 + (player.getStr() / 100))));
                }

                player.setArmor(selectedArmor);
                player.setStr((player.getStr()) + selectedArmor.getStr());
                player.setDex((player.getDex()) + selectedArmor.getDex());
                player.setIntel((player.getIntel()) + selectedArmor.getIntel());
                if(player.getWeapon() != null) {
                    player.setDps((player.getWeapon().getDps() * (1 + (player.getStr() / 100))));
                }else{ player.setDps((1 * (1 + (player.getStr() / 100))));
                }

                System.out.println("You have now equipped the armor piece: " + selectedArmor.getName());

                return false;

            }


        }
    }

    //Helper method for equipping weapon
    public boolean equipWep2(Player player, Weapon selectedWeapon) throws InvalidWeaponException {

        if(selectedWeapon == null){
            throw new InvalidWeaponException("Weapon does not exist");
        }else if(player.getLevel() < selectedWeapon.getReqLvl()){
            throw new InvalidWeaponException("Your level is not high enough to use this weapon");
        }

        else{

            if (player.getWeapon() != null) { //If player already have a weapon equipped, remove attributes from player before adding a new one
                player.setDex((player.getDex() - player.getWeapon().getDex()));
                player.setStr((player.getStr() - player.getWeapon().getStr()));
                player.setIntel((player.getIntel() - player.getWeapon().getIntel()));
                player.setDps((player.getWeapon().getDps() * (1 + (player.getDex() / 100))));

            }
            //Adding the new weapon.
            player.setWeapon(selectedWeapon);
            player.setHasWeapon();
            player.setStr((player.getStr()) + selectedWeapon.getStr());
            player.setDex((player.getDex()) + selectedWeapon.getDex());
            player.setIntel((player.getIntel()) + selectedWeapon.getIntel());
            player.setDps((player.getWeapon().getDps() * (1 + (player.getDex() / 100))));

            System.out.println("You have now equipped the weapon: " + selectedWeapon.getName());

            return false;
        }

    }

    //Getters and Setters

    public boolean getHasWeapon() {
        return this.hasWeapon;
    }

    public void setHasWeapon(){
        this.hasWeapon = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getStr() {
        return str;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public int getDex() {
        return dex;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;

    }

    public int getIntel() {
        return intel;
    }

    public void setIntel(int intel) {
        this.intel = intel;
    }

    public Armor getArmor(){
        return this.armor;
    }
    public void setArmor(Armor armor){
        this.armor = armor;
    }
}


