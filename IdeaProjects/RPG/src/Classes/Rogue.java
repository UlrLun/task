package Classes;

import Items.Armor;
import Items.Weapon;


public class Rogue extends Player {

    int level = 1;
    int dps;
    int dex = 6;
    Weapon wepList[] = new Weapon[3];
    Armor armorList[] = new Armor[10];
    Armor equippedArmor[] = new Armor[5];




    public Rogue(String name, int level, int str, int dex, int intel){
        super(name, level, str, dex, intel);
        this.dps = (this.level*(1+(this.dex/100)));


        wepList[0] = new Weapon("Basic Dagger", 1, "One-Hand", 10, 1, 1, 5, 1); //All existing weapon for Classes.Rogue
        wepList[1] = new Weapon("Advanced Sword", 5, "Two Hand", 40, 2, 1, 10, 1);
        wepList[2] = new Weapon("Master Sword", 10, "*Two Hand", 100,3, 1, 15, 1);

        armorList[0] = new Armor("Gloves", "Leather", 1, 1,4,1); //All armor existing for Classes.Rogue
        armorList[1] = new Armor("Boots", "Leather", 1, 1,4,1);
        armorList[2] = new Armor("Chest", "Leather", 1, 1,4,2);
        armorList[3] = new Armor("Legs", "Leather", 1, 1,4,2);
        armorList[4] = new Armor("Head", "Leather", 1, 1,4,2);
        armorList[5] = new Armor("Gloves2", "Mail", 5, 1,9,3);
        armorList[6] = new Armor("Boots2", "Mail", 5, 2,9,3);
        armorList[7] = new Armor("Chest2", "Mail", 5, 2,9,5);
        armorList[8] = new Armor("Legs2", "Mail", 5, 2,9,5);
        armorList[9] = new Armor("Head2", "Mail", 5, 2,9,5);



    }
    public int getDps(){
        return this.dps;
    }
    public void setDps(int i){
        this.dps = i;
    }

    public Weapon[] getWeaponList(){
        return this.wepList;
    }
    public Armor[] getArmorList(){
        return this.armorList;
    }
    public Armor[] getEquippedArmor(){
        return this.equippedArmor;
    }
    @Override
    public void levelUp(Player player){
        player.setLevel(player.getLevel() + 1);
        player.setDex(player.getDex() + 4);
        player.setStr(player.getStr() + 1);
        player.setIntel(player.getIntel() + 1);

        System.out.println("You have leveled up! Your are now lvl " + player.getLevel());
    }

    }
