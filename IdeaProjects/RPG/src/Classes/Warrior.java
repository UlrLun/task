package Classes;

import Items.Armor;
import Items.Weapon;

public class Warrior extends Player {

    int level = 1;
    int str = 5;
    int dps;
    Weapon wepList[] = new Weapon[3];
    Armor armorList[] = new Armor[10];
    Armor equippedArmor[] = new Armor[5];



    public Warrior(String name, int level, int str, int dex, int intel){
        super(name, level, str, dex, intel);

        this.dps = (this.level*(1+(this.str/100)));

        wepList[0] = new Weapon("Basic Hammer", 1, "One-Hand", 10, 1, 5, 1, 1);//All existing weapons for warrior
        wepList[1] = new Weapon("Advanced Sword", 5, "Two Hand", 40, 2, 10 ,1, 1);
        wepList[2] = new Weapon("Master Axe", 10, "*Two Hand", 100,3, 15, 1, 1);

        armorList[0] = new Armor("Gloves", "Mail", 1, 4,4,1); //All armor existing for Classes.Warrior
        armorList[1] = new Armor("Boots", "Mail", 1, 4,4,1);
        armorList[2] = new Armor("Chest", "Mail", 1, 6,4,2);
        armorList[3] = new Armor("Legs", "Mail", 1, 6,4,2);
        armorList[4] = new Armor("Head", "Mail", 1, 5,4,2);
        armorList[5] = new Armor("Gloves2", "Plate", 5, 8,2,2);
        armorList[6] = new Armor("Boots2", "Plate", 5, 8,2,2);
        armorList[7] = new Armor("Chest2", "Plate", 5, 11,2,2);
        armorList[8] = new Armor("Legs2", "Plate", 5, 11,2,2);
        armorList[9] = new Armor("Head2", "Plate", 5, 10,2,2);

    }
    public int getDps(){
        return this.dps;
    }
    public void setDps(int i){
        this.dps = i;
    }

    public Weapon[] getWeaponList(){
        return this.wepList;
    }
    public Armor[] getArmorList(){
        return this.armorList;
    }
    public Armor[] getEquippedArmor(){
        return this.equippedArmor;
    }
    @Override
    public void levelUp(Player player){
        player.setLevel(player.getLevel() + 1);
        player.setDex(player.getDex() + 2);
        player.setStr(player.getStr() + 3);
        player.setIntel(player.getIntel() + 1);

        System.out.println("You have leveled up! Your are now lvl " + player.getLevel());
    }

}
