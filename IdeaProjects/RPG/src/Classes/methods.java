package Classes;

import Classes.Player;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Armor;
import Items.Weapon;

public interface methods {

    //interface class with the Classes.methods that is required for children-classes of Classes.Player

    public void levelUp(Player player);
    public void equipWep(Player player)throws InvalidWeaponException;
    public boolean equipArmor2(Player player, Armor selectedArmor) throws InvalidArmorException;
    public boolean equipWep2(Player player, Weapon selectedWeapon) throws InvalidWeaponException;
    public void equipArmor(Player player) throws InvalidArmorException;
    public void addArmor(Armor armor);
    public boolean isArmorEquipped(Armor armor);
    public Armor getPreviousArmor(Armor armor);
    public Weapon[] getWeaponList();
    public Armor[] getArmorList();
    public Armor[] getEquippedArmor();
    public int getDps();
    public void setDps(int i);







    }
