package Exceptions;

public class InvalidArmorException extends Exception {

    public InvalidArmorException(String s){
        super(s);
    }
}
