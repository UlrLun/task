package Exceptions;

public class InvalidWeaponException extends Exception {

    public InvalidWeaponException(String s){
        super(s);
    }
}
