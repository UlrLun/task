package Items;

public class Armor {

    //Variables for any armor
    String name;
    String type;

    int level;

    int str;
    int dex;
    int intel;
    Armor equippedArmor[] = new Armor[5];


    //Constructor
    public Armor(String name, String type, int level, int str, int dex, int intel){
        this.name = name;
        this.type = type;
        this.level = level;
        this.str = str;
        this.dex = dex;
        this.intel = intel;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getStr() {
        return str;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public int getDex() {
        return dex;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public int getIntel() {
        return intel;
    }

    public void setIntel(int intel) {
        this.intel = intel;
    }

    public Armor[] getEquippedArmor() {
        return equippedArmor;
    }

    public void setEquippedArmor(Armor[] equippedArmor) {
        this.equippedArmor = equippedArmor;
    }
}
