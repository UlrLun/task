package Items;

public class Weapon {

    //Variables
    String name;
    int reqLvl;
    String slot;

    int str;
    int dex;
    int intel;

    int damage;
    int aps;
    int dps;


    //Constructor
    public Weapon(String name, int lvl, String slot, int damage, int aps, int str, int dex, int intel){

        this.name = name;
        this.reqLvl = lvl;
        this.slot = slot;
        this.damage = damage;
        this.aps = aps;
        this.dps = damage * aps;
        this.str = str;
        this.dex = dex;
        this.intel = intel;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getReqLvl() {
        return reqLvl;
    }

    public void setReqLvl(int reqLvl) {
        this.reqLvl = reqLvl;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public int getStr() {
        return str;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public int getDex() {
        return dex;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public int getIntel() {
        return intel;
    }

    public void setIntel(int intel) {
        this.intel = intel;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getAps() {
        return aps;
    }

    public void setAps(int aps) {
        this.aps = aps;
    }

    public int getDps() {
        return dps;
    }

    public void setDps(int dps) {
        this.dps = dps;
    }
}
