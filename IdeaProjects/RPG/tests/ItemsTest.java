import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Classes.*;
import Items.Armor;
import Items.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ItemsTest {

    @Test

    public void Create_CreateWeapon_ReturnsIfNameOfWeaponIsCorrect(){//Checks if weapon has correct name
        //Arrange
        String expected = "Basic Bow";

        //Act
        Weapon actual = new Weapon("Basic Bow", 1, "One-Hand", 10, 1, 1, 5, 1); //Weapons existing for Classes.Ranger

        //Assert
        assertEquals(expected, actual.getName());
    }
    @Test

    public void Create_CreateWeapon_ReturnsIfLevelOfWeaponIsCorrect(){//Checks if weapon har correct required level
        //Arrange
        int expected = 1;

        //Act
        Weapon actual = new Weapon("Basic Bow", 1, "One-Hand", 10, 1, 1, 5, 1); //Weapons existing for Classes.Ranger

        //Assert
        assertEquals(expected, actual.getReqLvl());
    }
    @Test

    public void Create_CreateWeapon_ReturnsIfDamageOfWeaponIsCorrect(){//Checks if the damage for the weapon is correct
        //Arrange
        int expected = 10;

        //Act
        Weapon actual = new Weapon("Basic Bow", 1, "One-Hand", 10, 1, 1, 5, 1);

        //Assert
        assertEquals(expected, actual.getDamage());

    }

    @Test
    public void Create_CreateWeapon_ReturnsIfAttacksPerSecondIsCorrect(){

        //Arrange
        int expected = 1;

        //Act
        Weapon actual = new Weapon("Basic Bow", 1, "One-Hand", 10, 1, 1, 5, 1);

        //Assert
        assertEquals(expected, actual.getAps());

    }
    @Test
    public void Create_CreateWeapon_ReturnsIfStrOfWeaponIsCorrect(){
         //Arrange
        int expected = 1;

        //Act
        Weapon actual = new Weapon("Basic Bow", 1, "One-Hand", 10, 1, 1, 5, 1);

        //Assert
        assertEquals(expected, actual.getStr());
    }
    @Test
    public void Create_CreateWeapon_ReturnsIfDexOfWeaponIsCorrect() {
        //Arrange
        int expected = 5;

        //Act
        Weapon actual = new Weapon("Basic Bow", 1, "One-Hand", 10, 1, 1, 5, 1);

        //Assert
        assertEquals(expected, actual.getDex());
    }
    @Test
    public void Create_CreateWeapon_ReturnsIfIntelOfWeaponIsCorrect() {
        //Arrange
        int expected = 1;

        //Act
        Weapon actual = new Weapon("Basic Bow", 1, "One-Hand", 10, 1, 1, 5, 1);

        //Assert
        assertEquals(expected, actual.getIntel());
    }

    @Test
    public void Calculate_CalcDps_ChecksDpsIfNoWeaponIsEquipped(){//Checking if dps is as expected with no weapon equipped

        //Arrange
        Warrior war = new Warrior("Ulrik", 1, 5, 2, 1);
        int expected = 1*(1+(5/100));


        //Act
        int actual = war.getDps();

        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void Calculate_CalcDps_CheckDpsWhenWeaponIsEquipped() throws InvalidWeaponException {

        //Arrange
        Warrior war = new Warrior("Ulrik", 1, 5, 2, 1);
        Weapon wep = new Weapon("Basic Hammer", 1, "One-Hand", 7, 1, 5, 1, 1);//All existing weapons for warrior

        int expected = (7*1)*(1+(5/100));
        //Act
        war.equipWep2(war, wep);


        //Assert

        assertEquals(expected, war.getDps());

    }

    @Test
    public void Calculate_CalcDps_CheckDpsWhenWeaponAndArmorIsEquipped() throws InvalidArmorException, InvalidWeaponException {

        //Arrange
        Warrior war = new Warrior("Ulrik", 1, 5, 2, 1);
        Weapon wep = new Weapon("Basic Axe", 1, "One-Hand", 7, 1, 5, 1, 1);//All existing weapons for warrior
        Armor armor = new Armor("Gloves", "Mail", 1, 1,1,3); //Create a new weapon
        int expected = (7*1) * (1+(5+1)/100);
        //Act

        war.equipArmor2(war, armor);
        war.equipWep2(war,wep);

        //Assert

        assertEquals(expected, war.getDps());
    }

    @Test
    public void Equip_EquipWeapon_ReturnExceptionIfNotHighEnoughLevel(){

        //Arranger
        Warrior war = new Warrior("Ulrik", 1, 5, 2, 1);
        Weapon wep = new Weapon("Basic Axe", 5, "One-Hand", 7, 1, 5, 1, 1);//All existing weapons for warrior
        String expected = "Your level is not high enough to use this weapon";

        //Act & Assert
        Exception exception = assertThrows(InvalidWeaponException.class, () -> war.equipWep2(war,wep));
        String actual = exception.getMessage();


        assertEquals(expected, actual);
    }

    @Test
    public void Equip_EquipValidWeapon_ReturnTrueIfEquipped() throws InvalidWeaponException {

        //Arrange
        boolean expected = true;
        Warrior war = new Warrior("Ulrik", 1, 5, 2, 1);
        Weapon wep = new Weapon("Basic Axe", 1, "One-Hand", 7, 1, 5, 1, 1);//All existing weapons for warrior

        //Act
        war.equipWep2(war, wep);

        //Assert
        assertEquals(expected, war.getHasWeapon());


    }

    @Test
    public void Equip_EquipArmor_ReturnExceptionIfNotHighEnoughLevel(){

        //Arrange

        Warrior war = new Warrior("Ulrik", 1, 5, 2, 1);
        Armor armor = new Armor("Gloves", "Mail", 5, 1,1,3); //Create a new weapon
        String expected = "You do not have the level requirement for this armor";

        //Act & Arrange

        Exception exception = assertThrows(InvalidArmorException.class, () -> war.equipArmor2(war,armor));
        String actual = exception.getMessage();

        assertEquals(expected, actual);

    }

}
