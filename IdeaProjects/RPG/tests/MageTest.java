import Classes.Mage;
import Items.Armor;
import Items.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    @Test
    public void create_CreateMage_ShouldCrateAMageWithCorrectLevel(){//Checks level of mage on creation

        //Arrange

        int expected = 1;

        //Act
        Mage actual = new Mage("Ulrik", 1, 1, 1, 8);

        //Assert
        assertEquals(expected, actual.getLevel());


    }
    @Test
    public void create_CreateMageStr_ShouldCreateAMageWithCorrectStr(){ //Checks str of mage on creation

        //Arrange
        int expected = 1;

        //Act
        Mage mage = new Mage("Ulrik", 1, 1, 1, 8);

        //Assert

        assertEquals(expected, mage.getStr());
    }
    @Test
    public void create_CreateMageDex_ShouldCreateAMageWithCorrectDex(){//Checks dex of mage on creation

        //Arrange
        int expected = 1;

        //Act
        Mage mage = new Mage("Ulrik", 1, 1, 1, 8);

        //Assert

        assertEquals(expected, mage.getDex());
    }
    @Test
    public void create_CreateMageIntel_ShouldCreateAMageWithCorrectIntel(){//Checks intelligence on mage on creation

        //Arrange
        int expected = 8;

        //Act
        Mage mage = new Mage("Ulrik", 1, 1, 1, 8);

        //Assert

        assertEquals(expected, mage.getIntel());
    }



    @Test
    public void levelUp_levelUpMage_ShouldUpgradeLevelAndAttributesOfPlayer(){ //Checking if the mage levels up when levelUp() is called.

        //Arrange
        Mage mage = new Mage("Ulrik", 1, 1, 1, 8);

        int expected = 2;
        //Act
        mage.levelUp(mage);

        //Assert
        assertEquals(expected, mage.getLevel()); //Checks if the mage has leveled up to the expected level


    }
    @Test
    public void levelUp_levelUpStr_ShouldReturnStrengthForMage(){//Checks if mage has correct str after levelup

        //Arrange
        Mage mage = new Mage("Ulrik", 1, 1, 1, 8);
        int expected = 2;

        //Act
        mage.levelUp(mage);

        //Assert

        assertEquals(expected, mage.getStr());


    }
    @Test
    public void levelUp_levelUpDex_ShouldReturnDexForMage(){//Checks if mage has correct dex after levelup

        //Arrange
        Mage mage = new Mage("Ulrik", 1, 1, 1, 8);
        int expected = 2;

        //Act
        mage.levelUp(mage);

        //Assert

        assertEquals(expected, mage.getDex());


    }
    @Test
    public void levelUp_levelUpIntel_ShouldReturnIntelligenceForMage(){//Checks if mage has correct intelligence after levelup

        //Arrange
        Mage mage = new Mage("Ulrik", 1, 1, 1, 8);
        int expected = 13;

        //Act
        mage.levelUp(mage);

        //Assert

        assertEquals(expected, mage.getIntel());


    }
    @Test
    public void equip_equipWeapon_ShouldEquipAGivenWeaponToThePlayer(){//Checks if weapon was equipped to mage

        //Arrange

        Mage mage = new Mage("Ulrik", 1, 1, 1, 1); //Create a new mage
        Weapon expected = new Weapon("Basic Wand", 1, "hand", 10, 1, 1, 1, 5); //Create weapon

        //Act
        mage.setWeapon(expected); //Equips the "expected" weapon

        //Assert
        assertEquals(expected, mage.getWeapon()); //Checks if the given weapon is the same as the mage equipped

    }
    @Test
    public void equip_equipArmor_ShouldEquipArmor(){//Checks if armor was equipped

        //Arrange
        Armor expected = new Armor("Gloves", "Cloth", 1, 1,1,3); //Create a new weapon
        Mage mage = new Mage("Ulrik", 1, 1, 1, 1); //Create a new mage

        //Act
        mage.setArmor(expected); //Sets the "expected" armour as the newest armour piece for the mage

        //Assert
        assertEquals(expected, mage.getArmor()); //Checks if the weapon "expected" is the same at the mage has just equipped



    }

}