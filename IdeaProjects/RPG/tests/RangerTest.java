import Classes.Ranger;
import Items.Armor;
import Items.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RangerTest {

    @Test
    public void create_CreateRanger_ShouldCrateARangerWithCorrectLevel(){ //Checks level of ranger on creation

        //Arrange

        int expected = 1;

        //Act
        Ranger actual = new Ranger("Ulrik", 1, 1, 7, 1);

        //Assert
        assertEquals(expected, actual.getLevel());


    }
    @Test
    public void create_CreateRangerStr_ShouldCreateARangerWithCorrectStr(){//Checks str of ranger on creation

        //Arrange
        int expected = 1;

        //Act
        Ranger ranger = new Ranger("Ulrik", 1, 1, 7, 1);

        //Assert

        assertEquals(expected, ranger.getStr());
    }
    @Test
    public void create_CreateRangerDex_ShouldCreateARangerWithCorrectDex(){//Checks dex of ranger on creation

        //Arrange
        int expected = 7;

        //Act
        Ranger ranger = new Ranger("Ulrik", 1, 1, 7, 1);

        //Assert

        assertEquals(expected, ranger.getDex());
    }
    @Test
    public void create_CreateRangerIntel_ShouldCreateARangerWithCorrectIntel(){//Checks intelligence of ranger on creation

        //Arrange
        int expected = 1;

        //Act
        Ranger ranger = new Ranger("Ulrik", 1, 1, 7, 1);

        //Assert

        assertEquals(expected, ranger.getIntel());
    }



    @Test
    public void levelUp_levelUpRanger_ShouldUpgradeLevelOfPlayer(){ //Checking if the Classes.Ranger levels up when levelUp() is called.

        //Arrange
        Ranger ranger = new Ranger("Ulrik", 1, 1, 7, 1);

        int expected = 2;
        //Act
        ranger.levelUp(ranger);

        //Assert
        assertEquals(expected, ranger.getLevel()); //Checks if the Classes.Ranger has leveled up to the expected level


    }
    @Test
    public void levelUp_levelUpStr_ShouldReturnStrengthForRanger(){//Checks if str is correct after levelup

        //Arrange
        Ranger ranger = new Ranger("Ulrik", 1, 1, 7, 1);
        int expected = 2;

        //Act
        ranger.levelUp(ranger);

        //Assert

        assertEquals(expected, ranger.getStr());


    }
    @Test
    public void levelUp_levelUpDex_ShouldReturnDexForRanger(){//Checks if dex is correct after levelup

        //Arrange
        Ranger ranger = new Ranger("Ulrik", 1, 1, 7, 1);
        int expected = 12;

        //Act
        ranger.levelUp(ranger);

        //Assert

        assertEquals(expected, ranger.getDex());


    }
    @Test
    public void levelUp_levelUpIntel_ShouldReturnIntelligenceForRanger(){//Checks if intelligence is correct after levelup

        //Arrange
        Ranger ranger = new Ranger("Ulrik", 1, 1, 7, 1);
        int expected = 2;

        //Act
        ranger.levelUp(ranger);

        //Assert

        assertEquals(expected, ranger.getIntel());


    }
    @Test
    public void equip_equipWeapon_ShouldEquipAGivenWeaponToThePlayer(){//Checks if ranger equipped the weapon given

        //Arrange

        Ranger ranger = new Ranger("Ulrik", 1, 1, 7, 1);
        Weapon expected = new Weapon("Basic Bow", 1, "One-Hand", 10, 1, 1, 5, 1); //Weapons existing for Classes.Ranger

        //Act
        ranger.setWeapon(expected); //Equips the "expected" weapon

        //Assert
        assertEquals(expected, ranger.getWeapon()); //Checks if the given weapon is the same as the Classes.Ranger equipped

    }
    @Test
    public void equip_equipArmor_ShouldEquipArmor(){//Checks equipping of armor

        //Arrange
        Armor expected = new Armor("Gloves", "Cloth", 1, 1,1,3); //Create a new weapon
        Ranger ranger = new Ranger("Ulrik", 1, 1, 7, 1);

        //Act
        ranger.setArmor(expected); //Sets the "expected" armour as the newest armour piece for the Classes.Ranger

        //Assert
        assertEquals(expected, ranger.getArmor()); //Checks if the weapon "expected" is the same at the Classes.Ranger has just equipped



    }
}
