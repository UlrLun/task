import Classes.Rogue;
import Items.Armor;
import Items.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RogueTest {

    @Test
    public void create_CreateRogue_ShouldCrateARogueWithCorrectLevel(){ //Checks level on creation

        //Arrange

        int expected = 1;

        //Act

        Rogue actual = new Rogue("Ulrik", 1, 2, 6, 1);

        //Assert
        assertEquals(expected, actual.getLevel());


    }
    @Test
    public void create_CreateRogueStr_ShouldCreateARogueWithCorrectStr(){//Checks str on creation

        //Arrange
        int expected = 2;

        //Act
        Rogue rogue = new Rogue("Ulrik", 1, 2, 6, 1);

        //Assert

        assertEquals(expected, rogue.getStr());
    }
    @Test
    public void create_CreateRogueDex_ShouldCreateARogueWithCorrectDex(){ //Checks dex on creation

        //Arrange
        int expected = 6;

        //Act
        Rogue rogue = new Rogue("Ulrik", 1, 2, 6, 1);

        //Assert

        assertEquals(expected, rogue.getDex());
    }
    @Test
    public void create_CreateRogueIntel_ShouldCreateARogueWithCorrectIntel(){//Checks intelligence on creation

        //Arrange
        int expected = 1;

        //Act
        Rogue rogue = new Rogue("Ulrik", 1, 2, 6, 1);

        //Assert

        assertEquals(expected, rogue.getIntel());
    }



    @Test
    public void levelUp_levelUpRogue_ShouldUpgradeLevelOfPlayer(){ //Checking if the rogue levels up when levelUp() is called.

        //Arrange
        Rogue rogue = new Rogue("Ulrik", 1, 2, 6, 1);

        int expected = 2;
        //Act
        rogue.levelUp(rogue);

        //Assert
        assertEquals(expected, rogue.getLevel()); //Checks if the rogue has leveled up to the expected level


    }
    @Test
    public void levelUp_levelUpStr_ShouldReturnStrengthForRogue(){//Checks if str is correct after levelup

        //Arrange
        Rogue rogue = new Rogue("Ulrik", 1, 2, 6, 1);
        int expected = 3;

        //Act
        rogue.levelUp(rogue);

        //Assert

        assertEquals(expected, rogue.getStr());


    }
    @Test
    public void levelUp_levelUpDex_ShouldReturnDexForRogue(){//Checks if dex is correct after levelup

        //Arrange
        Rogue rogue = new Rogue("Ulrik", 1, 2, 6, 1);
        int expected = 10;

        //Act
        rogue.levelUp(rogue);

        //Assert

        assertEquals(expected, rogue.getDex());


    }
    @Test
    public void levelUp_levelUpIntel_ShouldReturnIntelligenceForRogue(){//Checks intelligence after levelup

        //Arrange
        Rogue rogue = new Rogue("Ulrik", 1, 2, 6, 1);
        int expected = 2;

        //Act
        rogue.levelUp(rogue);

        //Assert

        assertEquals(expected, rogue.getIntel());


    }
    @Test
    public void equip_equipWeapon_ShouldEquipAGivenWeaponToThePlayer(){//Checks if rogue equipped weapon correctly

        //Arrange

        Rogue rogue = new Rogue("Ulrik", 1, 2, 6, 1);
        Weapon expected = new Weapon("Basic Bow", 1, "One-Hand", 10, 1, 1, 5, 1); //Weapons existing for rogue

        //Act
        rogue.setWeapon(expected); //Equips the "expected" weapon

        //Assert
        assertEquals(expected, rogue.getWeapon()); //Checks if the given weapon is the same as the rogue equipped

    }
    @Test
    public void equip_equipArmor_ShouldEquipArmor(){//Checks if rogue equipped armor

        //Arrange
        Armor expected = new Armor("Gloves", "Cloth", 1, 1,1,3); //Create a new weapon
        Rogue rogue = new Rogue("Ulrik", 1, 2, 6, 1);

        //Act
        rogue.setArmor(expected); //Sets the "expected" armour as the newest armour piece for the rogue

        //Assert
        assertEquals(expected, rogue.getArmor()); //Checks if the weapon "expected" is the same at the rogue has just equipped



    }
}
