import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Classes.Warrior;
import Items.Armor;
import Items.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarriorTest {

    @Test
    public void create_CreateWarrior_ShouldCrateAWarriorWithCorrectLevel(){ //Checks level of warrior on creation

        //Arrange

        int expected = 1;

        //Act
        Warrior actual = new Warrior("Ulrik", 1, 5, 2, 1);

        //Assert
        assertEquals(expected, actual.getLevel());


    }
    @Test
    public void create_CreateWarriorStr_ShouldCreateAWarriorWithCorrectStr(){ //Checks str on warrior on creation

        //Arrange
        int expected = 5;

        //Act
        Warrior warrior = new Warrior("Ulrik", 1, 5, 2, 1);

        //Assert

        assertEquals(expected, warrior.getStr());
    }
    @Test
    public void create_CreateWarriorDex_ShouldCreateAWarriorWithCorrectDex(){ //Checks dex on warrior after creation

        //Arrange
        int expected = 2;

        //Act
        Warrior warrior = new Warrior("Ulrik", 1, 5, 2, 1);

        //Assert

        assertEquals(expected, warrior.getDex());
    }
    @Test
    public void create_CreateWarriorIntel_ShouldCreateAWarriorWithCorrectIntel(){ //Checks intelligence on warrior on creation.

        //Arrange
        int expected = 1;

        //Act
        Warrior warrior = new Warrior("Ulrik", 1, 5, 2, 1);

        //Assert

        assertEquals(expected, warrior.getIntel());
    }



    @Test
    public void levelUp_levelUpWarrior_ShouldUpgradeLevelOfPlayer(){ //Checking if the warrior levels up when levelUp() is called.

        //Arrange
        Warrior warrior = new Warrior("Ulrik", 1, 5, 2, 1);

        int expected = 2; //Expected level on warrior after levelup
        //Act
        warrior.levelUp(warrior); //Level up the warrior

        //Assert
        assertEquals(expected, warrior.getLevel()); //Checks if the warrior has leveled up to the expected level


    }
    @Test
    public void levelUp_levelUpStr_ShouldReturnStrengthForWarrior(){ //Checks if str was correctly added on levelup

        //Arrange
        Warrior warrior = new Warrior("Ulrik", 1, 5, 2, 1);
        int expected = 8; //Expected str after levelup

        //Act
        warrior.levelUp(warrior); //Level up the warrior

        //Assert

        assertEquals(expected, warrior.getStr());


    }
    @Test
    public void levelUp_levelUpDex_ShouldReturnDexForWarrior(){ //Checks if dex was correctly added on levelup

        //Arrange
        Warrior warrior = new Warrior("Ulrik", 1, 5, 2, 1);
        int expected = 4; //Expected dex for warrior after levelup

        //Act
        warrior.levelUp(warrior); //Levels up the warrior

        //Assert

        assertEquals(expected, warrior.getDex());


    }
    @Test
    public void levelUp_levelUpIntel_ShouldReturnIntelligenceForWarrior(){ //Checks if intelligence was correctly added on levelup

        //Arrange
        Warrior warrior = new Warrior("Ulrik", 1, 5, 2, 1);
        int expected = 2; //Expected intelligence after levelup

        //Act
        warrior.levelUp(warrior); //levels up the created warrior

        //Assert

        assertEquals(expected, warrior.getIntel());


    }
    @Test
    public void equip_equipWeapon_ShouldEquipAGivenWeaponToThePlayer() throws InvalidWeaponException {//Checks if a weapon gets added to warrior

        //Arrange

        Warrior warrior = new Warrior("Ulrik", 1, 5, 2, 1);
        Weapon expected = new Weapon("Basic Hammer", 1, "One-Hand", 10, 1, 1, 5, 1); //Items.Weapon existing for warrior

        //Act


            warrior.equipWep2(warrior, expected); //Equips the "expected" weapon


        //Assert
        assertEquals(expected, warrior.getWeapon()); //Checks if the given weapon is the same as the warrior equipped

    }
    @Test
    public void equip_equipArmor_ShouldEquipArmorToPlayer() throws InvalidArmorException { //Checks if armor was added to warrior

        //Arrange
        Armor expected = new Armor("Gloves", "Mail", 1, 1,1,3); //Create a new weapon
        Warrior warrior = new Warrior("Ulrik", 1, 5, 2, 1);

        //Act

            warrior.equipArmor2(warrior,expected); //Sets the "expected" armour as the newest armour piece for the warrior


        //Assert
        assertEquals(expected, warrior.getArmor()); //Checks if the weapon "expected" is the same at the warrior has just equipped



    }
}
