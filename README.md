# What is this?

This project is a console application in java. It is a RPG based "game" where you can choose a character. You can level up, equip items and view a stats page. The user of the program is asked for some inputs along the way, you have to choose what class you want to be, and what your name shuold be. After that you have to write commands as to what the next action should be, do you want to level up (Train)? View Stats page (Stats)? All the commands are listed in the console as you play the game. 

# Getting the application
To run this application, i reccomend using intellij. Pull the project to your computer from gitlab and run the main in Character.java.


# Structure
The folders in the application is structured as such:
```
├───src
│   ├───Classes
│   ├───Exceptions
│   └───Items
└───tests
```


Where "Classes" contains the java-classes for different class types in the game, aswell as a parentclass Player.java wich impliments most of the functionality for this application.

The "Exceptions" folder contains a few custom exception that have been used in the code.

"Items" contains 2 classes, Weapon.java and Armor.java. There two classes construct the items and helps the other classes get information about the items.


# Author
This application is developed by Ulrik.
